# TEST 

- A list of names is stored in a variable `sort_this_array` in javascript.
- When user click on the button (with text `Click Me!`), it should display this list of names sorted by name.

# Setup git

- Create an account in gitlab.com

- Install git and configure it if you have not already https://help.github.com/articles/set-up-git/#setting-up-git

- Clone repository via terminal

  ```
  git clone https://gitlab.com/bhargav-sae/Diala-Test.git
  ```

- Go inside the project folder
  
  ```
  cd Diala-Test
  ```

- Open `index.html` in a browser.

- You will need to update `index.html` file to accomplish the given task.


# Helpful links

Javscript Turorials https://www.w3schools.com/js/default.asp

JavaScript click events https://www.w3schools.com/jsref/event_onclick.asp

JavaScript sort https://www.w3schools.com/jsref/jsref_sort.asp

Css Tutorials
https://www.w3schools.com/css/css3_intro.asp

HTML Tutorials
https://www.w3schools.com/html/html_intro.asp

# Submiting solution

- Add files

  ```
  git add .
  ```

- Commit the solution

  ```
  git commit -m "<what changes you made describe in few words>"
  ```
- Submit it
  
  ```
  git push
  ```


**Do not hesitate to ask for any help or contact us for any questions.**